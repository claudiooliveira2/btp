import {Injectable} from '@angular/core';
import {Component} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export interface FeedItem {
  description: string;
  category: string;
  id: string;
  count: number;
  createdAt: any;
  answers: any[];
  displayName: string;
  photoURL: string;
}

@Injectable({
  providedIn: 'root'
})
export class FeedService {
  
  feeds: AngularFirestoreCollection<FeedItem>;
  items: Observable<{}[]>;

  constructor(public db: AngularFirestore) {
    this.feeds = db.collection<FeedItem>('feeds');
    this.items = db.collection('feeds', ref => ref.orderBy('createdAt', 'desc')).snapshotChanges()
      .pipe(
       map(actions => actions.map(a => {
         const data = a.payload.doc.data() as FeedItem;
         const id = a.payload.doc.id;
         return { id, ...data };
       }))
     );
  }

  add(feed: FeedItem) {
    return this.feeds.add(feed);
  }

  update(feed) {
    return this.db.doc('feeds/' + feed.id).update(feed).then((ref) => {
      return ref;
    });
  }
}
