import {Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import {FeedItem, FeedService} from './feed.service';
import {Observable} from 'rxjs';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { MatSnackBar } from '@angular/material';

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import { first } from 'rxjs/operators';

export interface DialogData {
  description: string,
  category: string,
  createdAt: null,
  count : number,
  answers : any[],
  id: string,
  displayName : string,
  photoURL : string
};

import * as moment from 'moment';

import 'moment/locale/pt'  // without this line it didn't work
moment.locale('pt')

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'btp';

  isSaving = false;

  feedback = {
    description: '',
    category: '',
    createdAt: null,
    count : 0,
    answers : [],
    displayName : '',
    photoURL : '',
    id : ''
  };

  categories = [{
    id: '1',
    name: "Cais"
  }, {
    id: '2',
    name: "Pátio"
  }, {
    id: '3',
    name: 'Administrativo'
  }, {
    id: '4',
    name: 'Outro'
  }];

  feedItems: Observable<{}[]>;

  constructor(private feedService: FeedService, public dialog: MatDialog, public afAuth: AngularFireAuth) {
    this.feedItems = feedService.items;
  }

  isLoggedIn() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  getCategoryName(categoryId) {
  	var c = this.categories.filter((category) => {
  		return category.id == categoryId;
  	});
  	return (c.length ? c[0].name : 'Sem categoria');
  }

  login() {
    return this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  upVote(item) {
    if (!item.count) {
      item.count = 0;
    }

    if (item.hasVotedUp) {
      return;
    }

    item.count++;
    if (!item.hasVotedDown) item.hasVotedUp = true;
    item.hasVotedDown = false;
  }

  downVote(item) {
    if (!item.hasVotedDown) {
      item.count--;
      if (!item.hasVotedUp) item.hasVotedDown = true;
      item.hasVotedUp = false;
    }
  }

  newPost(user) {

    if (!this.feedback.description) {
      return;
    } else if (!this.feedback.category) {
      return;
    }

    this.isLoggedIn().then((res) => {
       if (res) {
         this.feedback.displayName = user.displayName;
         this.feedback.photoURL = user.photoURL;
         this.savePost();
       }else{
         this.login().then((u : any) => {
           this.feedback.displayName = u.user.displayName;
           this.feedback.photoURL = u.user.photoURL;
           this.savePost();
         });
       }
    });

  }

  savePost() {
    this.isSaving = true;

    Object.assign(this.feedback, {
      createdAt: new Date(),
      count: Math.floor(Math.random() * 50)
    });

    delete this.feedback.id;

    this.feedService.add(this.feedback);
    this.isSaving = false;

    this.feedback = {
      description: '',
      category: '',
      createdAt: null,
      count : 0,
      answers : [],
      displayName : '',
      photoURL : '',
      id : ''
    };

    this.isSaving = false;
  }

  randomAvatar() {
  	if (Math.floor(Math.random() * 30) / 2 == 1) {
  		return 'https://randomuser.me/api/portraits/women/' + Math.floor(Math.random() * 30) + '.jpg';
  	}else{
	  	return 'https://randomuser.me/api/portraits/men/' + Math.floor(Math.random() * 30) + '.jpg';
	  }
  }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

	response = {
		text : '',
		createdAt : null,
		count : 0,
    displayName : '',
    photoURL : ''
	};

	isSaving = false;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    private feedService: FeedService,
    private snackBar: MatSnackBar,
    public afAuth: AngularFireAuth,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  sendResponse(user) {

  	if (!this.response.text) {
  		return;
  	}

    this.isLoggedIn().then((res) => {
       if (res) {
          this.isSaving = true;

          if (!this.data.answers) {
            this.data.answers = [];
          }

          this.response.createdAt = new Date();
          this.response.displayName = user.displayName;
          this.response.photoURL = user.photoURL;

          this.savePost();

       }else{
        this.login().then((u : any) => {
          this.response.createdAt = new Date();
          this.response.displayName = u.user.displayName;
          this.response.photoURL = u.user.photoURL;
          this.savePost();
        });
       }
    });

  }

  savePost() {
    this.data.answers.push(this.response);

    this.feedService.update(this.data);
    this.isSaving = false;
    this.snackBar.openFromComponent(SnackResponseComponent, {
      duration: 3000,
    });
    this.response = {
      text : '',
      createdAt : null,
      count : 0,
      displayName : '',
      photoURL : ''
    };
  }

  isLoggedIn() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  upVote(item) {
    if (!item.count) {
      item.count = 0;
    }

    if (item.hasVotedUp) {
      return;
    }

    item.count++;
    if (!item.hasVotedDown) item.hasVotedUp = true;
    item.hasVotedDown = false;
  }

  downVote(item) {
    if (!item.hasVotedDown) {
      item.count--;
      if (!item.hasVotedUp) item.hasVotedDown = true;
      item.hasVotedUp = false;
    }
  }

  login() {
    return this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

}

@Component({
  selector: 'snack-response',
  templateUrl: 'snack-response.html',
})
export class SnackResponseComponent {}